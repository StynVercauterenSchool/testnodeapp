import {MigrationInterface, QueryRunner} from "typeorm";

export class myInit1586604726787 implements MigrationInterface {
    name = 'myInit1586604726787'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "book" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "title" character varying(300) NOT NULL, "description" character varying(300) NOT NULL, "author" character varying(300) NOT NULL, CONSTRAINT "PK_a3afef72ec8f80e6e5c310b28a4" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD "isActive" boolean NOT NULL DEFAULT true`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD "isArchived" boolean NOT NULL DEFAULT false`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD "createDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD "createdBy" character varying(300) NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD "lastChangedDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD "lastChangedBy" character varying(300) NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD "internalComment" character varying(300)`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "book" DROP COLUMN "internalComment"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" DROP COLUMN "lastChangedBy"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" DROP COLUMN "lastChangedDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" DROP COLUMN "createdBy"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" DROP COLUMN "createDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" DROP COLUMN "isArchived"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" DROP COLUMN "isActive"`, undefined);
        await queryRunner.query(`DROP TABLE "book"`, undefined);
    }

}
