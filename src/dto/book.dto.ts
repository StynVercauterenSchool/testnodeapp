import { ApiProperty } from '@nestjs/swagger'
import { IsString, IsUUID } from 'class-validator'
import { Book } from '../entities/book.entity'
import { User } from '../user.decorator'

export class BookDTO implements Readonly<BookDTO> {
  @ApiProperty({ required: true })
  @IsUUID()
  id: string


  @ApiProperty({ required: true })
  @IsString()
  title: string

  @ApiProperty({ required: true })
  @IsString()
  description: string

  @ApiProperty({ required: true })
  @IsString()
  author: string

  public static from(dto: Partial<BookDTO>) {
    const it = new BookDTO()
    it.id = dto.id
    it.title = dto.title
    it.description = dto.description
    it.author = dto.author
    return it
  }
  
  public static fromEntity(entity: Book) {
    return this.from({
      id: entity.id,
      title: entity.title,
      description: entity.description,
      author: entity.author
    })
  }

  //TODO: user instellen naar user van db ipv hardcoded
  public toEntity(user: User = null) {
    const it = new Book()
    it.id = this.id
    it.title = this.title
    it.description = this.description
    it.author = this.author
    it.createDateTime = new Date()
    it.createdBy = user ? user.id : "Styn"
    it.lastChangedBy = user ? user.id : "Styn"
    return it
  }
}