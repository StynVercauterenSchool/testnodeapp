import { PrimaryGeneratedColumn, Column, Entity } from 'typeorm'
import { BaseEntity } from './base.entity'

@Entity({ name: 'book' })
export class Book extends BaseEntity {
    @Column({ type: 'varchar', length: 300 })
    title: string

    @Column({ type: 'varchar', length: 300 })
    description: string

    @Column({ type: 'varchar', length: 300 })
    author: string
}
