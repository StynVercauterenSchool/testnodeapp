import * as _ from 'lodash'
import { createConnection, ConnectionOptions } from 'typeorm'
import { configService } from '../config/config.service'
import { User } from '../user.decorator'
import { BooksService } from '../books/books.service'
import { Book } from '../entities/book.entity'
import { BookDTO } from '../dto/book.dto'

async function run() {
  const seedUser: User = { id: 'seed-user' }

  const seedId = Date.now()
    .toString()
    .split('')
    .reverse()
    .reduce((s, it, x) => (x > 3 ? s : (s += it)), '')

  const opt = {
    ...configService.getTypeOrmConfig(),
    debug: true
  }

  const connection = await createConnection(opt as ConnectionOptions)
  const bookService = new BooksService(connection.getRepository(Book))

  const work = _.range(1, 10)
    .map(n => BookDTO.from({
      title: `seed${seedId}-${n}`,
      description: 'created from seed',
      author: 'Seed'
    }))
    .map(dto => bookService.addBook(dto, seedUser)
      .then(r => (console.log('done ->', r.title), r)))

  return await Promise.all(work)
}

run()
  .then(_ => console.log('...wait for script to exit'))
  .catch(error => console.error('seed error', error))