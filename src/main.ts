import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cors from 'cors' //Dependency to reach the server from website
import { configService } from './config/config.service';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  if (configService.isProduction()) {
    const document = SwaggerModule.createDocument(app, new DocumentBuilder()
      .setTitle('Book API')
      .setDescription('My custom Book API')
      .build())

    SwaggerModule.setup('docs', app, document)
  }

  app.use(cors())
  await app.listen(4000);
}
bootstrap();
