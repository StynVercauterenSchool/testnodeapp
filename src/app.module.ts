import { Module } from '@nestjs/common';
import { BooksModule } from './books/books.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from './config/config.service'

@Module({
  imports: [BooksModule, TypeOrmModule.forRoot(configService.getTypeOrmConfig())]
})
export class AppModule {}
