import { Controller, Get, Param, Post, Body, Query, Delete, ValidationPipe } from '@nestjs/common'
import { BooksService } from './books.service'
import { BookDTO } from '../dto/book.dto'
import { User } from '../user.decorator'

@Controller('api/books')
export class BooksController {
    constructor(private booksService: BooksService) { }

    @Get('')
    public async getBooks() {
        return await this.booksService.getBooks()
    }
/*
    @Get(':bookID')
    async getBook(@Param('bookID') bookID: number) {
        const book = await this.booksService.getBook(bookID)
        return book
    }
*/
    @Post()
    public async post(@User() user: User, @Body(new ValidationPipe({ transform:true })) createBookDTO: BookDTO): Promise<BookDTO>{
        return await this.booksService.addBook(createBookDTO, user)
    }

    /*
    @Delete()
    async deleteBook(@Query("bookID") id: number) {
        const books = await this.booksService.deleteBook(id)
        return books
    }
    */
}