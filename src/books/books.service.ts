import { Injectable, HttpException } from '@nestjs/common'
//import { BOOKS } from '../mocks/books.mock'
import { Repository } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm'
import { Book } from '../entities/book.entity'
import { BookDTO } from '../dto/book.dto'
import { User } from '../user.decorator'

@Injectable()
export class BooksService {
    constructor(@InjectRepository(Book) private readonly repo: Repository<Book>) {}
    //books = BOOKS

    public async getBooks(): Promise<BookDTO[]> {
        return await this.repo.find().then(books => books.map(book => BookDTO.fromEntity(book)))
    }
/*
    getBook(bookID: number): Promise<any> {
        return new Promise(resolve => {
            const book = this.books.find(book => book.id == bookID)
            if (!book) {
                throw new HttpException('Book does not exist!', 404)
            }
            resolve(book)
        })
    }
*/
    public async addBook(bookDto: BookDTO, user: User): Promise<BookDTO> {
        return this.repo.save(bookDto.toEntity(user)).then(book => BookDTO.fromEntity(book))
        /*
        return new Promise(resolve => {
            this.books.push(book)
            resolve(this.books)
        })
        */
    }
/*
    deleteBook(bookID: number): Promise<any> {
        return new Promise(resolve => {
            let index = this.books.findIndex(book => book.id == bookID)
            if (index === -1) {
                throw new HttpException('Book does not exist!', 404)
            }
            this.books.splice(1, index)
            resolve(this.books)
        })
    }
    */
}